//
//  main.c
//  mcls
//
//  Created by Eric S. Londres on 9/7/19.
//  Copyright © 2019 Eric S. Londres. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <unistd.h>

int main(int argc, const char * argv[]) {
    puts("Status: 200 OK\r");
    puts("Content-Type: text/html\r");
    puts("\r");
    puts("Hello, world!\n");
    return EXIT_SUCCESS;
}
